// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuetify from 'vuetify';
import VeeValidate from 'vee-validate';
import 'vuetify/dist/vuetify.min.css';
import App from './App';
import store from './store/store';

const veeValidateConfig = {
  locale: 'pl',
  dictionary: {
    pl: {
      messages: {
        required: fieldName => `Pole ${fieldName} nie może być puste`,
        min: fieldName => `Pole ${fieldName} jest zbyt krótkie`,
        max: fieldName => `Pole ${fieldName} jest zbyt długie`,
        regex: fieldName => `Pole ${fieldName} ma nieprawidłowy format`,
      },
    },
  },
};

Vue.use(Vuetify);
Vue.use(VeeValidate, veeValidateConfig);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  components: {
    App,
  },
  template: '<App/>',
});
