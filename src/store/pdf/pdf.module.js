const moduleState = () => ({
  pdfData: null,
});

const getters = {
  getPdfData: state => state.pdfData,
};

const mutations = {
  mutatePdfData: (state, payload) => {
    state.pdfData = payload;
  },

  resetState: (state) => {
    const initial = moduleState();
    Object.keys(initial).forEach((key) => {
      state[key] = initial[key];
    });
  },
};

const actions = {
  setPdfData: ({ commit }, payload) => {
    commit('mutatePdfData', payload);
  },

  resetModuleState({ commit }) {
    commit('resetState');
  },

  resetPdfStore({ dispatch }) {
    // Dispatch resetModuleState in this modules
    dispatch('resetModuleState');
  },
};

export default {
  namespaced: true,
  state: moduleState(),
  getters,
  mutations,
  actions,
};
