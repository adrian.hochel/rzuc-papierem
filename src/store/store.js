import Vue from 'vue';
import Vuex from 'vuex';
import form from './form/form.module';
import pdf from './pdf/pdf.module';

Vue.use(Vuex);

const actions = {
  resetStore({ dispatch }) {
    dispatch('form/resetFormStore');
    dispatch('pdf/resetPdfStore');
  },
};

const store = new Vuex.Store({
  strict: true,
  actions,
  modules: {
    form,
    pdf,
  },
});

export default store;
