import employee from './employee.form.module';
import employer from './employer.form.module';
import others from './others.form.module';

const moduleState = () => ({
  currentStep: 1,
  minStep: 1,
  totalSteps: 4,
});

const getters = {
  getCurrentStep: state => state.currentStep,
  getMinStep: state => state.minStep,
  getTotalSteps: state => state.totalSteps,
  getAllFormData: () => ({
    employee: employee.state,
    employer: employer.state,
    others: others.state,
  }),
};

const mutations = {
  mutateCurrentStep: (state, payload) => {
    state.currentStep = payload;
  },

  resetState: (state) => {
    const initial = moduleState();
    Object.keys(initial).forEach((key) => {
      state[key] = initial[key];
    });
  },
};

const actions = {
  setCurrentStep: ({ commit }, payload) => {
    commit('mutateCurrentStep', payload);
  },

  resetModuleState({ commit }) {
    commit('resetState');
  },

  resetFormStore({ dispatch }) {
    // Dispatch resetModuleState in this modules
    dispatch('resetModuleState');

    // Dispatch resetModuleState in nested modules
    dispatch('employee/resetModuleState');
    dispatch('employer/resetModuleState');
    dispatch('others/resetModuleState');
  },
};

const modules = {
  employee,
  employer,
  others,
};

export default {
  namespaced: true,
  state: moduleState(),
  getters,
  mutations,
  actions,
  modules,
};
