const moduleState = () => ({
  name: '',
  street: '',
  buildingNumber: '',
  city: '',
  postcode: '',
});

const getters = {
  getName: state => state.name,
  getStreet: state => state.street,
  getBuildingNumber: state => state.buildingNumber,
  getCity: state => state.city,
  getPostcode: state => state.postcode,
  getEmployerDetails: state => ({
    name: state.name,
    lastName: state.lastName,
    street: state.street,
    buildingNumber: state.buildingNumber,
    city: state.city,
    postcode: state.postcode,
  }),
};

const mutations = {
  mutateName: (state, payload) => {
    state.name = payload;
  },

  mutateStreet: (state, payload) => {
    state.street = payload;
  },

  mutateBuildingNumber: (state, payload) => {
    state.buildingNumber = payload;
  },

  mutateCity: (state, payload) => {
    state.city = payload;
  },

  mutatePostcode: (state, payload) => {
    state.postcode = payload;
  },

  resetState: (state) => {
    const initial = moduleState();
    Object.keys(initial).forEach((key) => {
      state[key] = initial[key];
    });
  },
};

const actions = {
  setName: ({ commit }, payload) => {
    commit('mutateName', payload);
  },

  setStreet: ({ commit }, payload) => {
    commit('mutateStreet', payload);
  },

  setBuildingNumber: ({ commit }, payload) => {
    commit('mutateBuildingNumber', payload);
  },

  setCity: ({ commit }, payload) => {
    commit('mutateCity', payload);
  },

  setPostcode: ({ commit }, payload) => {
    commit('mutatePostcode', payload);
  },

  resetModuleState({ commit }) {
    commit('resetState');
  },
};

export default {
  namespaced: true,
  state: moduleState(),
  getters,
  mutations,
  actions,
};
