const moduleState = () => ({
  firstName: '',
  lastName: '',
  street: '',
  flatNumber: '',
  city: '',
  postcode: '',
});

const getters = {
  getFirstName: state => state.firstName,
  getLastName: state => state.lastName,
  getStreet: state => state.street,
  getFlatNumber: state => state.flatNumber,
  getCity: state => state.city,
  getPostcode: state => state.postcode,
  getEmployeeDetails: state => ({
    firstName: state.firstName,
    lastName: state.lastName,
    street: state.street,
    flatNumber: state.flatNumber,
    city: state.city,
    postcode: state.postcode,
  }),
};

const mutations = {
  mutateFirstName: (state, payload) => {
    state.firstName = payload;
  },

  mutateLastName: (state, payload) => {
    state.lastName = payload;
  },

  mutateStreet: (state, payload) => {
    state.street = payload;
  },

  mutateFlatNumber: (state, payload) => {
    state.flatNumber = payload;
  },

  mutateCity: (state, payload) => {
    state.city = payload;
  },

  mutatePostcode: (state, payload) => {
    state.postcode = payload;
  },

  resetState: (state) => {
    const initial = moduleState();
    Object.keys(initial).forEach((key) => {
      state[key] = initial[key];
    });
  },
};

const actions = {
  setFirstName: ({ commit }, payload) => {
    commit('mutateFirstName', payload);
  },

  setLastName: ({ commit }, payload) => {
    commit('mutateLastName', payload);
  },

  setStreet: ({ commit }, payload) => {
    commit('mutateStreet', payload);
  },

  setFlatNumber: ({ commit }, payload) => {
    commit('mutateFlatNumber', payload);
  },

  setCity: ({ commit }, payload) => {
    commit('mutateCity', payload);
  },

  setPostcode: ({ commit }, payload) => {
    commit('mutatePostcode', payload);
  },

  resetModuleState({ commit }) {
    commit('resetState');
  },
};

export default {
  namespaced: true,
  state: moduleState(),
  getters,
  mutations,
  actions,
};
