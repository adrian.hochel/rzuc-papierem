const moduleState = () => ({
  documentCity: '',
  documentDate: '',
  agreementType: '',
  agreementDate: '',
  fullName: '',
});

const getters = {
  getDocumentCity: state => state.documentCity,
  getDocumentDate: state => state.documentDate,
  getAgreementType: state => state.agreementType,
  getAgreementDate: state => state.agreementDate,
  getFullName: state => state.fullName,
  getOtherDetails: state => ({
    documentCity: state.documentCity,
    documentDate: state.documentDate,
    agreementType: state.agreementType,
    agreementDate: state.agreementDate,
    fullName: state.fullName,
  }),
};

const mutations = {
  mutateDocumentCity: (state, payload) => {
    state.documentCity = payload;
  },

  mutateDocumentDate: (state, payload) => {
    state.documentDate = payload;
  },

  mutateAgreementType: (state, payload) => {
    state.agreementType = payload;
  },

  mutateAgreementDate: (state, payload) => {
    state.agreementDate = payload;
  },

  mutateFullName: (state, payload) => {
    state.fullName = payload;
  },

  resetState: (state) => {
    const initial = moduleState();
    Object.keys(initial).forEach((key) => {
      state[key] = initial[key];
    });
  },
};

const actions = {
  setDocumentCity: ({ commit }, payload) => {
    commit('mutateDocumentCity', payload);
  },

  setDocumentDate: ({ commit }, payload) => {
    commit('mutateDocumentDate', payload);
  },

  setAgreementType: ({ commit }, payload) => {
    commit('mutateAgreementType', payload);
  },

  setAgreementDate: ({ commit }, payload) => {
    commit('mutateAgreementDate', payload);
  },

  setFullName: ({ commit }, payload) => {
    commit('mutateFullName', payload);
  },

  resetModuleState({ commit }) {
    commit('resetState');
  },
};

export default {
  namespaced: true,
  state: moduleState(),
  getters,
  mutations,
  actions,
};
