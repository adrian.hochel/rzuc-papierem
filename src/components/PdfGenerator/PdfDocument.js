import PDFDocument from 'pdfkit';
import blobStream from 'blob-stream';
import axios from 'axios';

/**
 * Simple wrapper for pdfkit with third-party fonts support
 */
export default class PdfGenerator {
  constructor(config = {}) {
    const defaultConfig = {
      fonts: [
        {
          fontName: 'Lora-Regular',
          fontUrl: '/static/pdf-fonts/Lora-Regular.ttf',
        },
      ],
      defaultFont: 'Lora-Regular',
    };

    this.config = {
      ...defaultConfig,
      ...config,
    };

    this.downloadedFonts = [];
  }

  /**
   * Creates new pdf document
   *
   * @returns {Promise}
   */
  async newDocument() {
    // create a document the same way as above
    this.doc = new PDFDocument();

    // pipe the document to a blob
    this.stream = this.doc.pipe(blobStream());

    // Wait for font download
    await this.applyFonts(this.config.fonts);

    // Set default font
    this.doc.font(this.config.defaultFont);
  }

  /**
   * Downloading and applying third-party fonts into document
   *
   * @param {Array} fonts Array with all fonts used in document
   *
   * @returns {Promise}
   */
  async applyFonts(fonts) {
    // Filter fonts to be download to don't download again the same fonts
    const fontsToDownload = fonts.filter(
      allFonts =>
        !this.downloadedFonts.find(
          downloadedFonts => downloadedFonts.fontName === allFonts.fontName,
        ),
    );

    if (fontsToDownload.length > 0) {
      // Download missing fonts
      await this.getFonts(fontsToDownload);
    }

    // Register all third-party fonts in document
    this.downloadedFonts.map(font =>
      this.doc.registerFont(font.fontName, font.fontFile),
    );
  }

  /**
   * Download all given fonts and return Promise when all fonts are downloaded
   *
   * @param {Array} fonts Array with fonts which needs to be downloaded
   *
   * @returns {Promise}
   */
  getFonts(fonts) {
    const promises = fonts.map(font =>
      this.getFont(font.fontName, font.fontUrl),
    );
    return Promise.all(promises);
  }

  /**
   * Download given font and save its data into downloadedFonts Array
   *
   * @param {String} fontName A name that will be used in document
   * @param {String} fontUrl Absolute path for font in .ttf format
   *
   * @returns {Promise}
   */
  getFont(fontName, fontUrl) {
    return axios
      .get(fontUrl, {
        responseType: 'arraybuffer',
      })
      .then((response) => {
        this.downloadedFonts.push({ fontName, fontFile: response.data });
      });
  }

  /**
   * Stop document creation, finish stream
   */
  endDocument() {
    this.doc.end();
  }

  /**
   * Get a blob with pdf document
   *
   * @returns {Promise}
   */
  getBlob() {
    return new Promise((resolve) => {
      this.stream.on('finish', () => {
        // get a blob so you can do whatever you like with
        // const blob = this.stream.toBlob ('application/pdf');

        // get a blob URL for display in the browser
        resolve(this.stream.toBlobURL('application/pdf'));
      });
    });
  }
}
