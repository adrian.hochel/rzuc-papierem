import PdfDocument from './PdfDocument';

/**
 * Adds content generation methods to PdfDocument
 */
export default class PdfGenerator extends PdfDocument {
  constructor(config = {}) {
    const defaultConfig = {
      generateSectionIfEmpty: false,
    };

    const mergedConfig = {
      ...defaultConfig,
      ...config,
    };

    // invoke parent with merged config
    super(mergedConfig);

    // assign section generators
    this.sectionGenerators = PdfGenerator.defineSectionGenerators();
  }

  /**
   * Checks if all object properties are empty
   * @param object
   * @returns {boolean}
   */
  static arePropertiesEmpty(object) {
    const objectValues = Object.values(object);

    return objectValues.every(el => !el);
  }

  /**
   * Decides if section should be generated based on section data
   * @param sectionData
   * @returns {boolean}
   */
  isSectionDataValid(sectionData) {
    const generateSectionIfEmpty = this.config.generateSectionIfEmpty;

    return generateSectionIfEmpty
      ? true
      : !PdfGenerator.arePropertiesEmpty(sectionData);
  }

  /**
   * Creates section in PDFDocument using defined section generator
   * @param {String} sectionName
   * @param {Object} sectionData
   * @returns {boolean} wasSectionGenerated
   */
  generateSection(sectionName, sectionData) {
    let wasSectionGenerated = false;
    // check if we have such section generator and validate given section data
    if (
      {}.hasOwnProperty.call(this.sectionGenerators, sectionName) &&
      this.isSectionDataValid(sectionData)
    ) {
      this.sectionGenerators[sectionName].call(this, sectionData);

      wasSectionGenerated = true;
    }

    return wasSectionGenerated;
  }

  /**
   * Return object with content generator methods
   * @returns {Object}
   */
  static defineSectionGenerators() {
    return {
      dateAndPlace(formData) {
        this.doc
          .fontSize(12)
          .text(`${formData.documentCity}, dnia ${formData.documentDate}`, {
            align: 'right',
            paragraphGap: 20,
          });
      },

      employee(formData) {
        this.doc
          .text(`${formData.firstName} ${formData.lastName}`)
          .text(`${formData.street} ${formData.flatNumber}`)
          .text(`${formData.postcode} ${formData.city}`)
          .moveDown();
      },

      employer(formData) {
        const alignRight = { align: 'right' };

        this.doc
          .text(formData.name, alignRight)
          .text(`${formData.street} ${formData.buildingNumber}`, alignRight)
          .text(`${formData.postcode} ${formData.city}`, alignRight)
          .moveDown();
      },

      heading(formData) {
        this.doc
          .fontSize(25)
          .text(`Wypowiedzenie umowy ${formData.agreementType}`, {
            align: 'center',
            paragraphGap: 20,
          });
      },

      content(formData) {
        this.doc
          .fontSize(12)
          .text(
            `Niniejszym wypowiadam umowę o pracę zawartą dnia ${
              formData.agreementDate
            }, pomiędzy firmą ${formData.companyName}, a ${
              formData.fullName
            } z zachowaniem okresu wypowiedzenia przewidzianego w umowie.`, {
              align: 'justify',
              indent: 30,
              ellipsis: false,
            },
          );
      },
    };
  }
}
