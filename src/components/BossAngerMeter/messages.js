export default [
  {
    step: 1,
    type: 'info',
    text:
      'Oferujemy 400 zł podwyżki za pozostanie w firmie. Rozważ propozycję.',
  },
  {
    step: 2,
    type: 'info',
    text:
      'Jesteś dobrym pracownikiem. Chcemy budować z Tobą zespół. Podwyżka 1000 zł. Zostajesz?',
  },
  {
    step: 3,
    type: 'warning',
    text:
      'Nasza firma bardzo Cię potrzebuje. Oferujemy 3000 zł podwyżki. Oferta limitowana czasowo.',
  },
  {
    step: 4,
    type: 'error',
    text:
      'Jesteś słaby technicznie, nie potrzebujemy Cię w naszym zespole.',
  },
];
